import React from 'react';
//import Buttom from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Typography from '@material-ui/core/Typography';
import AccountBoxIcon from '@material-ui/icons/AccountBox';

const AuthElements =  function(props){

    const logInButton = ()=>{

        if(props.user) return(
            [<Avatar key='auth-avatar-element' src={props.user.providerData[0].photoURL} />,
                <Typography key='auth-avatar-userName' className={props.classes.padding}>{props.user.displayName}</Typography>,
            (<IconButton key='auth-sing-out-button' color="inherit" onClick={props.logout}><ExitToApp /></IconButton>)]
        );
        return (
            <IconButton key='auth-sing-out-button' color="inherit" onClick={props.login}><AccountBoxIcon /> <Typography> Iniciar Sesión</Typography> </IconButton>
        //<Buttom variant="contained" onClick={props.login}>
        //Iniciar sesión con Google
        //</Buttom>
        )
    }

    return(
        <div className={props.classes.container}>
            {logInButton()}
        </div>
    );
}

export default withStyles({
    container:{
        display: 'flex',
        flexDirection: 'row'
    },
    padding:{
        paddingTop:'11px',
        marginLeft: '14px'
    }
})(AuthElements);

