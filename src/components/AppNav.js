import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Buttom from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Login from '../containers/Login';


class AppNav extends Component {
    render(){
        return(
            <div>
                <AppBar position="static">
                    <Toolbar>
                         <Typography variant="h6" component="h1" className={this.props.classes.grow}>
                             <Buttom color="inherit">
                                 Albums
                            </Buttom> 
                         </Typography>
                         <Login/>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

export default withStyles({
    grow:{flexGrow:1,textAlign:'left'}
})(AppNav);