import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAo11SUFQjaWhCExY43SZIqcy2OapvnEVQ",
    authDomain: "proyecto-albums.firebaseapp.com",
    databaseURL: "https://proyecto-albums.firebaseio.com",
    projectId: "proyecto-albums",
    storageBucket: "proyecto-albums.appspot.com",
    messagingSenderId: "385542812222",
    appId: "1:385542812222:web:bad9084a00046b2b49d76f"
  };

firebase.initializeApp(config);

export default firebase