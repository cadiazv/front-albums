export default {
    "albums": [
      {
        "id": "AMDRrVyM77e3X3W3oh9n3pQaC384-otPemJ1zDesMfA8bUsPtNyM9DXWy0PSfq3MY9_au8HJ15Q6",
        "title": "La tarde en Quilpué",
        "productUrl": "https://photos.google.com/lr/album/AMDRrVyM77e3X3W3oh9n3pQaC384-otPemJ1zDesMfA8bUsPtNyM9DXWy0PSfq3MY9_au8HJ15Q6",
        "mediaItemsCount": "10",
        "coverPhotoBaseUrl": "https://lh3.googleusercontent.com/lr/AFBm1_bydK3nglLC3Xu539W3Dcf3HkAVmGjM4L96hS_MsvA6gpHZlTQ3zIy_TlokyoBiegaetixwL6xPlVqIySF8RJ6QWqVX9HCsRQSurXZ1FwtvfEhqZFKdwKjHpYJGagZSj7tp7gJmj9rks-HIOLE5dNEBWd_YTFq_jQpwDxF2Lp3cLtywZyyylrXXDamS-wVZbWHSram1SE7isX9E1zncYadTyHoocTsGrMBnYZLbL-dWU0cXyxADKs0pv0StixEsxtylw4VVQlBbghqZLS7o9Vh1YKvgRjIlijchXmnFPgkBTPgj3PfwQacQTp657zNZzVzVtGiLqNzJXLRuOg8IhkQYuPC7Gf1a2b6YiNJwVQt623d-RjP9kCLqlxy2cZZVumVusQk2K3-Q8HBCNs6SAkLx7s396ibApyZrXhPEJdbt-IDFsfHKDn8si7pi1_b55GiIg5MsBX_1X3zpnMFwwYYsHHbZL3ihgBgfaaqCSvZGof0q7d0mLSh85Z6WTDuPLHS38J6T-dFEfX41-IGcpru4aOMiN-tpH5I1sBrQ1i0PMWvWQhx8NsWnmuqSenlqBdDwgKR3dCW0jFzd48XfpzWJaXnFjaxpcN4pDKQGVPczukE2NGPiabwDOh3rDkodsC00PBFD3Gcna4R76G-OQdTwq7eNS2gjQmvRPP3CYObv4piBKZD93uIDq7iHQm1UUvRd2CETomHZQbD2rRcoKq2S_W0kYExyN9GjfNS0UeYPJXW9QWH4zFBr_xkfxsT3UHgtgGORlkomfBQaP3Q-pWeUaGAJJpddbOA-gBhambSSvTD2rIPLTFrhri8YMVwV1od8",
        "coverPhotoMediaItemId": "AMDRrVzyuEzyU6V5SfIdkJ8YZT7WPPBpsxfzrTO7oaMnkajduDm7czOgU1yS8Rz5U6JS5KYJ8kUs7Aoo3PutL7Fng-fvxbodVA"
      },
      {
        "id": "AMDRrVxwPsKLbpJbs6z_uM4utlC6rxBq5gyQPV9DYWdNIqIB0Yf-AAwbcLtBXcAmgwZeoIylGye7",
        "title": "El jueves en Quilpué",
        "productUrl": "https://photos.google.com/lr/album/AMDRrVxwPsKLbpJbs6z_uM4utlC6rxBq5gyQPV9DYWdNIqIB0Yf-AAwbcLtBXcAmgwZeoIylGye7",
        "mediaItemsCount": "10",
        "coverPhotoBaseUrl": "https://lh3.googleusercontent.com/lr/AFBm1_bSihE-nwKWGDK8eFXTCNHKeo7CByG6yYJt3bWsegXMaBomH_jn1KcrTPgYp0sJwb2iDs48K4pdJmu-8_Fp-dgPn30Wfj0lihsj61WUVv3pyL5nnxgDpxVtcH09Atl8cIL1htmRjnzaISUe7W3HtfUPaanqkknxi8cadDTstGIyDujuN2lq7tm6Sk24kaOAfxs4pP_FTwYKypTPN4OAkKNumBjK1hbHTHHcQ4T1xJe-YWBNpgsKcWB4MFIJSbC7wwpvgiz9BXdWA1kBvXgCse4v4ROC_pQlDU_A72kHcZFMVirjDe54hqnprPWwL636uwEWHuUzUFzppNEsr_UCQsxkYu4GFK7bXYZUXmHVYXnTjRqpUTNN4T2-EXdZh49DTNUhTWI4oPyEkpuHcFTEFegazU4v76MvnYWq1ZJVW_UIa9wz_WmwnOAzJKqx9lMmJQgcFQo-HURrf45vlsfqVe91a5bhGWo6rOnU9QC1XqJ1gI50O_izvg8FdsknmgDYfoJ70wdC6U3CrKilIXWVRuX3yOtnu6lJ-S6JJnHYLAHDW7I4xviNVd34uIc2rT8TwSt7TuLJIOqCntJE7QAuzJQWZc3Mfr3om5JiQDe2Snd5dI4Fk1eoFvxynA3V3DKl_aWoRfslliYBBOceOb-1BtRa1FITAY1ChpL1QBFSebMEWc_F_zPCVZqg4-xZ3G57PCxCgM_DxzdilGBI71B_oFiJUTyrrYNVyVEUYvO-GnwOsJ9HN0NYro604b15eLD90WyaucMbVdYbjk0X7U6f0Qd-rdgrd0wEWB4oX4-ufpJWxufQd7tcdPqB7f6myB0tQ-Bl",
        "coverPhotoMediaItemId": "AMDRrVzEXdd3QEvf4e2YnuuV4aeTNlNA0tt2IfCqWPjSrX_QIhcoyc753OgB5wdogg66SAs-JYyAt6HjHewnK4UeSoGZuIyU5g"
      },
      {
        "id": "AMDRrVxTljE4gv0WllfwrD9fN_xhyRR7oFM6gv90gkwrlWTUQRfUP0O4rY1iD9CAjLpYxw5LhUdf",
        "title": "El sábado en Quilpué",
        "productUrl": "https://photos.google.com/lr/album/AMDRrVxTljE4gv0WllfwrD9fN_xhyRR7oFM6gv90gkwrlWTUQRfUP0O4rY1iD9CAjLpYxw5LhUdf",
        "mediaItemsCount": "12",
        "coverPhotoBaseUrl": "https://lh3.googleusercontent.com/lr/AFBm1_bG0W0jB_HrSE-wMOm1ufRwIG_gMKyRYcA6umE1R1k6MVOtRdFEo4tJcj7o55yoSXE3JweY_fp_Ge9DrbLIFA57KvovGbZ5sWbjfb0bkbIadLuXikuFox5OC1cvfNSwL-cLD7QV3GxtNlJoPNgE2um_-MYyYmWDQzWrJ68wmA1eSTR09PZ4Sd2FLbrGtGxvK7uJQ4Jhjt089zD5SAOIVZpyS6IK1i6PRM81cSScAcrcdNVbJYeJneQcdEWT7W9HZxCQTxu-luKb6N0ptpyp4vzRUjC8y2rHZmiKQ9iEQb-S8DqHO69tb4Oqa7NXqXYwvNnYdRU2Ae9OfZe1ixsxB5A5uZCjp1qbsljPld8B113oxF-oFl7Chh1TYLwwapzm4Mvo14yE33m6zBQuFPjxbRj8QvlIKHDp9s3UVt5bQxlNWVXGjYtC-KxcXjN21JmklbuNffLuie4qpazVkHsaUnEOXzuaxJQRRz30012MmliNxc_Dq-ZBUqqNYfID-TLClpeRSl2DpJMrKp11F_YLzoQjQ_aSNJaPV5VGw9_4THqkWncMnliYensPT3HVAd2C4X1_5564h_eyabA2JFFUmWAPiF7wBCCnOdk14E0cf1h9s1b87FlGnzIukjeYRVyoAdsNFGM03y7koJtxaNgl-DdoyOss1p_i6pEROUgBT-Xrhh7urW85GfdZf3tfVesTsq6JcWvQ6lA0KSJQ06Rmv5EwnwF5WMeh7MDVotBmF6tzxB6KsCQjV5whaCAadA2aWOE-e1XzB24H7VDqMwHF32md0e4QeQAlRiuKSedHnKDcPokM9PsakNRLEuW9HIEaoMLK",
        "coverPhotoMediaItemId": "AMDRrVy_ZjBD0QetpfSF_sp8ogXr0PJ43epw6QRpr_ogcHqCOiK1J4bPBMTEGzf5DtRAhxRiz6F7jv6fS1yvsKv2442_6ztTWw"
      },
      {
        "id": "AMDRrVycN_LTw4fz4u1PN_OrJTrvJ60mpfHjUyOL0yO6MLYegSPlAkZjqRd4CtsX3wY8sX4MlDkt",
        "title": "Martes en El Quisco",
        "productUrl": "https://photos.google.com/lr/album/AMDRrVycN_LTw4fz4u1PN_OrJTrvJ60mpfHjUyOL0yO6MLYegSPlAkZjqRd4CtsX3wY8sX4MlDkt",
        "mediaItemsCount": "14",
        "coverPhotoBaseUrl": "https://lh3.googleusercontent.com/lr/AFBm1_Y36dQhqn1EBxO5huOvUhvxAFkU0EV7DPjdPK8Y5vXoyXebdYSAVrQX0LIExuz8WKmLSsOAlA_NmnDh6g58g4E_QbC6Ga5xxVVKCJkQ0nhSyusM7lgcL6yw72NJUfSNtAy5ETq5-vwTN0Kt9yQtKjLTz9TkcZmdqcYgMDKEQDmmDltxUk5ktqq4UrH8XfMZylXMN6yC0Ov1L3Md5QSxkx-RP2-IyOUrqVxcaQDyePA9clmgWLe8tSjj9f27srNtw7fZF5OQ4PiV_tRlr8M193Tw93L3qDqwTAFP-qt1pTus7EaSFpOfow_i8pI__CHCUssrk5-T_Uld4jo9sfE_GjduiEWOXW160xoQjNw377YvcrKeQtHh2UsoaSGCOkc5M8k5ZxPd-mRFItxNq06PR-Iqxe_zSJ8BPJ4GOJaxTtkD6gpO6Hqi0X6tZIlFgygkbTf5sp3ArYpWZ06sTm-vSCKUQxn5ikeB3P4BA1MSLdy7-sx3kTjWRAHW5aXtOPeWCbTh0qMvy2nt01m3D1emhqjUZOyeflJbL-JFsuhuchDA8y2a_jwwttj5LeFcVLq9p0NfxzNYfXIiy39pqPDkbuXouJrLV_57WCrVEsyyHpkcoOjS587t4A8mYDUXmK6LZkATWYry0n3T6lVqWk84WZpGgWXYlf9njvjh18TeCIvvBBuLx_VK5v67OVqdJczo3-45zLloftzSrneR99gptBuxWU6WEWDMaHsXIkuvjvCyJ8Nda7XZGdrTgAwonPlLNicHc8LFYeeY46rC3VnYYBahaUHm0GCwpYH3U-hK3AX80f5BV0DvVAI222BUzihsoD8",
        "coverPhotoMediaItemId": "AMDRrVyuCV2Nc1eGpRToHHpX34l3POF5RKpzyJtp8dA5ttUfXQwMq9utRtmQUnYngZB2wu07IpUxzsJe2DiB4MRQECN_oWphIg"
      },
      {
        "id": "AMDRrVwLfZ0OT4z3-r-pAG6bMiAM5Uh_-N2qeXRr8oLd0LeK7WvYoCkIbEp_Qt5_KiiNXZmPTmxO",
        "title": "27 de agosto de 2013",
        "productUrl": "https://photos.google.com/lr/album/AMDRrVwLfZ0OT4z3-r-pAG6bMiAM5Uh_-N2qeXRr8oLd0LeK7WvYoCkIbEp_Qt5_KiiNXZmPTmxO",
        "mediaItemsCount": "1",
        "coverPhotoBaseUrl": "https://lh3.googleusercontent.com/lr/AFBm1_Z_gNr53NzooqEizqTaaQe-DJGCFn5OH4MH7qe7opQ1vy46CBIQAQRhkf65eQtYPxRjXYuQW8az-CEfBn7DU9rpPLQWjofnvNnDam3k1TuUqHf651UmyBFDcIg54l2FV6XGUfVebsFBWWlZK2TB-c1ezOgR2jUMCZEHfoMxV0DQUkBj0-ZD4iBkHzTejIV5uEQDyaeyestvdEXbH1FMMguJHGY7oRwDT57j52bVhMTCKCDRee1rEHlDyCkXr8oI6i_V7ycd1HtIE6JNShrWXgWKszRYtnMfBU-CrvUY9rAMgPac8Z9nFwgqXJGcFufjr0kOC22l7cLQ-aWmM_Zi-TAsS6RhRkyaj1h5PoJenLPnXpnDy9B_GsjDgOaKMCgn_hEZbHy7HSPRdtD-8hw2sYgm6H6VBP0Gy9_Nj_GZz-zyD8TP6w_aD7-E9R2exttLuDIcmTG2eoXQW0903tQIe3FLfwRdwRy9enBjkPe9G_3YcBRG129HDfkpPAAYKUhc3AYftjq7UZCb65IYxflHSizf6SpptwsywcMhP1BYj0wlYPqpfw8ipQdslW7Y_Ijx1YQiH5HKAFuR1g5ZXtqHhdC6fIaijK0JvsBDYdJ2bD_6kfY9HvRJ4J7j6oSnJIOKhkPoHQvKH6qiPi-irGLq_qyck2ZtooKeRZyDJhT8a-w0GYpSnhjZHIy84u0axZo1eMgiSGDstgA2PXBNixEa69an4EROf8BS9QJyrcm7JgkPH9hfyMZE28x58QnIPN352J7zvmVWC71at5n-N3YR8ViFteN8B_yQg-kymp9qa5vENvfXeH52AUG_9CwOM-kJbf8E",
        "coverPhotoMediaItemId": "AMDRrVy-xqJKYTwE5CqbiMgq7vaYKBKg8dS2EGmlh0doOjR1RIhiJUbgWFTPINify4U0Q0Tl266ubAd5xLrhg5sUcsN4-1l0Fw"
      }
    ]
  }